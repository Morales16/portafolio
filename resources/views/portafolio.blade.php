<html>
<head>
    <!--CSS-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!--JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</head>
<body>

<header class="header">
    <div class="container logo-nav-container">
        <nav class="navigation">
            <ul class="show">
                <li><a href="/home">Home</a></li>
                <li><a href="/about">Sobre mi</a></li>
                <li><a href="/portafolio">Portafolio</a></li>
                <li><a href="/contact">Contacto</a></li>
            </ul>
        </nav>
    </div>
</header>

<div class="container-sobremi">
    <u><h2>PORTAFOLIO</h2></u>
    <br>
    <br>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/laptop.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/mas.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
