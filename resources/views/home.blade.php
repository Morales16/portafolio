<html>
<head>
    <!--CSS-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!--JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</head>
<body>

<header class="header">
    <div class="container logo-nav-container">
        <nav class="navigation">
            <ul class="show">
                <li><a href="/home">Home</a></li>
                <li><a href="/about">Sobre mi</a></li>
                <li><a href="/portafolio">Portafolio</a></li>
                <li><a href="/contact">Contacto</a></li>
            </ul>
        </nav>
    </div>

    <div class="home">
        <h1>Hola soy, <span class="style">Fernando Morales</span><br>&lt; Desarrollador Web /&gt;</h1>
    </div>
</header>

</body>
</html>
