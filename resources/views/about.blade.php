<html>
<head>
    <!--CSS-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!--JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</head>
<body>

<header class="header">
    <div class="container logo-nav-container">
        <nav class="navigation">
            <ul class="show">
                <li><a href="/home">Home</a></li>
                <li><a href="/about">Sobre mi</a></li>
                <li><a href="/portafolio">Portafolio</a></li>
                <li><a href="/contact">Contacto</a></li>
            </ul>
        </nav>
    </div>
</header>
<div class="container-sobremi">
    <u><h2>SOBRE MI</h2></u>
    <br>
    <p>Soy orgullosamente de un pueblo en Nayarit, México. Comencé con mi carrera en TICs en el 2017 y estoy por graduarme. Tengo un especial interés por el desarrollo web y móvil por lo que he realizado algunos proyectos para aprender.</p>
    <br>
    <br>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/cel.png')}}" width="70" height="70">
                    <h5 class="card-title"><u><h5>Mobile Development</h5></u></h5>
                    <br>
                        <p class="card-text">En mi proceso he desarrollado algunas aplicaciones móviles, aunque aún tengo que profundizar más en este tema.</p>
                        <h5 class="card-title"><u><h5>Tecnologías</h5></u></h5>
                        <p class="card-text">Javascript, Ionic, Cordova</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/laptop.png')}}" width="70" height="70">
                    <h5 class="card-title"><u><h5>Frontend Development</h5></u></h5>
                    <br>
                        <p class="card-text">No estoy muy familiarizado pero me gustaria aprender más. Estoy trabajando pricipalmente en tecnologías para el front.</p>
                        <h5 class="card-title"><u><h5>Tecnologías</h5></u></h5>
                        <p class="card-text">HTML5, CSS3, Bootstrap, Angular</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <img src="{{asset('images/mas.png')}}" width="70" height="70">
                    <h5 class="card-title"><u><h5>Backend Development</h5></u></h5>
                    <br>
                        <p class="card-text">Tecnologías que me han servido para poder realizar mis aplicaciones.</p>
                        <h5 class="card-title"><u><h5>Tecnologías</h5></u></h5>
                        <p class="card-text">NodeJs, MongoDB, Express, PHP(Básico), MySQL</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
